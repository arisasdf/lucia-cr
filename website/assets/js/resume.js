!function(){
  // Fill the stars
  // ==============

  const FULL_STAR_HTML = '<svg class="icon"><use xlink:href="#icon-star-full"></use></svg>',
    EMPTY_STAR_HTML = '<svg class="icon"><use xlink:href="#icon-star-empty"></use></svg>';

  for (let item of document.getElementsByClassName('stars')) {
    let numberOfStars = parseInt(item.dataset['stars']),
      numberOfEmpties = 5 - numberOfStars,
      newInnerHtml = '';
    for (let i = 0; i < numberOfStars; i++) {
      newInnerHtml += FULL_STAR_HTML;
    }
    for (let i = 0; i < numberOfEmpties; i++) {
      newInnerHtml += EMPTY_STAR_HTML;
    }

    item.innerHTML = newInnerHtml;
  }

  // Action on description toggles
  document.querySelectorAll('.timeline__description-title').forEach(($el) => {
    $el.addEventListener('click', () => {
      $el.querySelector('.timeline__description-arrow').classList.toggle('timeline__description-arrow--flipped');
      $el.nextElementSibling.classList.toggle('timeline__description-extended--open');
    } )
  });
}();
